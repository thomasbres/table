import React from "react";
import ReactDOM from "react-dom";
import ZebulonTableDemo from "./demo/ZebulonTable.demo";

ReactDOM.render(<ZebulonTableDemo />, document.getElementById("root"));
